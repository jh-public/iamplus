from collections import OrderedDict

# raw data input file formats
metadata = OrderedDict([
    (
        "movie_titles_metadata.txt",
        {
            "fields": [
                "movieID",
                "movie_title",
                "movie_year",
                "IMDB_rating",
                "IMDB_votes",
                "genres",
            ],
            "key": [
                "movieID",
            ],
        },
    ),
    (
        "movie_characters_metadata.txt",
        {
            "fields": [
                "characterID",
                "character_name",
                "movieID",
                "movie_title",
                "gender",
                "position_in_credits",
            ],
            "key": [
                "characterID",
            ],
        },
    ),
    (
        "movie_lines.txt",
        {
            "fields": [
                "lineID",
                "characterID",
                "movieID",
                "character_name",
                "utterance",
            ],
            "key": [
                "lineID",
            ],
        },
    ),

])


# data-file file names
datafiles = {
        "movie":                "movie_titles_metadata.json",
        "characters":           "movie_characters_metadata.json",
        "lines":                "movie_lines.json",
        "conversations":        "movie_conversations.json",
        "conversation_lines":   "conversation_lines.json",
}
