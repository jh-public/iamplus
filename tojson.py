#!/usr/bin/env python3
from collections import namedtuple
from pdb         import set_trace as st
from metadata    import metadata, datafiles

try:
    import ujson as json
except ImportError:
    import json


separator=' +++$+++ '

def get_key( key_fields, record ):
    # construct record key
    return '-'.join(
        record [field]
        for field in key_fields
    )


def to_json( filename, meta, separator=separator ):
    # output to json file format
    print( filename )
    Record = namedtuple( 'record', meta ['fields'] )

    with open( "data/" + filename, encoding='latin-1' ) as f_in:

        # split line
        data = (
            line.strip( '\n' ).split( separator )
            for line in f_in
        )

        # make line records
        data = (
            Record._make( line )._asdict()
            for line in data
        )

        # get output data
        data = {
            get_key( meta ['key'], record ):
            record
            for record in data
        }

        # output to json
        outfile = 'data/%s.json' % filename.split( '.' )[0]
        with open( outfile, 'w' ) as f_out:
            json.dump( data, f_out, indent=2 )


def conversations_to_json():
    # add conversations to Lines json data file

    def get_lines_data( data_type='lines' ):
        with open( 'data/' + datafiles [data_type] ) as f:
            return json.load( f )

    def get_conversation_data( filename='movie_conversations.txt', separator=separator ):
        with open( 'data/' + filename ) as f:
            return [
                line.strip( '\n' ).split( separator )
                for line in f
            ]

    def get_conversation_records():
        Record = namedtuple(
            'record',
            [
                "characterID1",
                "characterID2",
                "movieID",
                "utterances",
            ],
        )
        return (
            Record._make( conversation )._asdict()
            for conversation in get_conversation_data()
        )

    def get_lines( conversations ):
        # add 'conversation' key to lines dictionary
        lines = get_lines_data()
        for conversation in conversations:
            conversation['utterances'] = eval( conversation['utterances'] )
            for line_id in conversation['utterances']:
                lines [line_id] ['conversation'] = conversation
        return lines

    def write_lines( data ):
        # output to json data file
        filename = datafiles ['lines']
        with open( 'data/' + filename, 'w' ) as f:
            json.dump( data, f, indent=2 )

    conversation_records = get_conversation_records()
    lines = get_lines( conversation_records )
    write_lines( lines )


if __name__ == '__main__':
    _ = [
        to_json( filename, meta )
        for filename, meta in metadata.items()
    ]
    _ = conversations_to_json()
