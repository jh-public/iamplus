#!/usr/bin/env python3
from collections import defaultdict, OrderedDict
from pdb         import set_trace as st
from pprint      import pprint
from metadata    import datafiles

import itertools
import operator

try:
    import ujson as json # 2x faster than standard json
except ImportError:
    import json


def get_data( data_type ):
    # load json data file
    with open( 'data/' + datafiles [data_type] ) as f:
        return json.load( f )


def get_line_groups( lines ):
    # group same lines together

    # sort lines
    lines = sorted([
        (
            line_info ['utterance'],
            line_id,
        )
        for line_id, line_info in lines.items()
    ])
    # group lines
    lines = itertools.groupby( lines, operator.itemgetter(0) )
    return  {
        line: [ line_id for _, line_id in line_ids ]
        for line, line_ids in lines
    }


def sort_line_groups( line_groups ):
    # sort line groups by number of occurrences
    return sorted(
        line_groups.items(),
        key = lambda item: len( item[1] ),
        reverse = True,
    )


def get_most_frequent_lines( lines, count=None ):
    # get the most frequent lines
    line_groups = get_line_groups( lines )
    data = sort_line_groups( line_groups )
    if count:
        data = data[:count]
    return data


def get_line_responses( line_id, all_lines ):
    # get all responses to a line
    conversation_lines = all_lines [line_id] ['conversation'] ['utterances']
    line_index = conversation_lines.index( line_id )
    try:
        response_line_id = conversation_lines[ line_index + 1 ]
    except IndexError:
        # print( 'No response for line %s: %s' % ( line_id, all_lines [line_id] ['utterance'] ) )
        return None
    return ( response_line_id, all_lines [response_line_id] )


def get_most_frequent_responses( line, line_ids, all_lines ):
    # get most frequent response(s)
    # all response lines with the same number of occurrences as the top response are included
    lines = (
        get_line_responses( line_id, all_lines )
        for line_id in line_ids
    )
    lines = { x[0]: x[1] for x in lines if x is not None }
    most_frequent_lines = get_most_frequent_lines( lines )

    data      = []
    first_len = len( most_frequent_lines [0] [1] )
    for _line in most_frequent_lines:
        if len( _line [1] ) < first_len:
            break
        data.append( _line )
    return data


def get_results( most_frequent_lines, all_lines ):
    # output most frequent lines and responses together
    most_frequent_responses = (
        get_most_frequent_responses( line, line_ids, all_lines )
        for line, line_ids in most_frequent_lines
    )
    return list(
        OrderedDict([
            ( 'line', line ),
            ( 'top_responses', top_responses ),
        ])
        for line, top_responses in
            zip( most_frequent_lines, most_frequent_responses )
    )


def write_data( data, filename='results.json' ):
    # write raw results data to json file
    with open( filename, 'w' ) as f:
        json.dump( data, f, indent=2 )


def write_results( data, filename='results.txt' ):
    # write formatted results to text file
    results = []
    for line_info in data:
        # line
        results.append(
            'Line:  %s' % line_info ['line'] [0]
        )

        total_replies = len( line_info ['top_responses'] )
        if total_replies == 1:
            results.append(
                'Reply: %s' % line_info ['top_responses'] [0] [0]
            )
        else:
            results.append( 'Replies:' )

            for i, reply in enumerate( line_info ['top_responses'] ):
                if i > 9:
                    results.append(
                        '     (%d more replies)...' % ( total_replies - 10 )
                    )
                    break
                else:
                    results.append(
                        '     - %s' % reply [0]
                    )

        results.append( '' )

    with open( filename, 'w' ) as f:
        f.write( '\n'.join( results ) )
    return results


def main():
    all_lines = get_data( 'lines' )
    most_frequent_lines = get_most_frequent_lines( all_lines, 25 )
    data = get_results( most_frequent_lines, all_lines )
    write_data( data )
    results = write_results( data )
    print( '\n'.join( results ) )


if __name__ == '__main__':
    _ = main()
