# I Am Plus Technical Test

Please find the most frequent dialogs (top 25) in the whole set of conversations that we are providing below.
For each most frequent dialog, find the most frequent reply to that dialog. Solution has to be implemented in Python.
Please find a corpus of movie conversations here: [ZIP File](http://www.mpi-sws.org/~cristian/data/cornell_movie_dialogs_corpus.zip).

## Notes
* There are some lines with multiple most frequent replies (same number of occurrences).  An arbitrary 10 replies are shown.
* Results includes blank lines for the movie character (no text in the movie-lines data file).

## METHODS
1. Data files are converted to JSON format using tojson.py.
2. Results are generated using technical_test.py.  Two data files are generated:
   * Results raw data: results.json
   * Formatted results: results.txt

##Results: Most Frequent Replies of Most Frequent Lines

```
Line:  What?
Reply: Nothing.

Line:  No.
Reply: Why not?

Line:  Yes.
Reply: Why?

Line:  Yeah.
Reply: Why?

Line:  Why?
Replies:
     * I don`t know.
     * Why what?

Line:  I don`t know.
Reply: You don`t know.

Line:  Okay.
Reply: Okay.

Line:  Thank you.
Reply: You`re welcome.

Line:  Who?
Replies:
     * Me!
     * I don`t know.

Line:  Sure.
Replies:
     * Thanks.
     * Good.

Line:
Reply:

Line:  Yeah?
Reply: Yeah.

Line:  Why not?
Reply: Because I don`t want one.

Line:  What is it?
Replies:
     * Listen.
     * Thank you for seeing me.
     * Just some fatherly advice.

Line:  Yes?
Reply: Be careful.

Line:  What do you mean?
Replies:
     * You know what I mean babe, It`s enough. We can stop.
     * "An Affair To Remember." Did you ever see it? Cary Grant and Deborah Kerr.  Before that it was called "Love Affair With Irene Dunne And Charles Boyer."
     * Somebody...
     * Well... the crew is confined to the ship when we land at Clavius. We have to stay inside for the time it take to refit - about twenty-four hours. And then we`re going to back empty.
     * Nothin`. Forget it.
     * You know it and I know it.
     * The place we`re going is on the other side of that.
     * Just that... you ask and I`ll answer.
     * Let`s just say Greg likes tackling tight ends on and off the field.
     * It`s a tough game.  Hard to pin that bastard down.  Keeps on changing the rules.
     (193 more replies)...

Line:  I know.
Reply: How do you know?

Line:  Hi.
Reply: Hi.

Line:  Right.
Reply: Brindisi versus Electric Boat.

Line:  Huh?
Replies:
     * You shouldn`t be carrying that heavy bag. I`ll take it.
     * To an artistic eye, you understand. Have you ever modeled?
     * They`re movin`.
     * Who the fuck is Arthur Digby Sellers?
     * Asshole.
     * I`ve gotten the `sackaroo` in many ways - but never in rhyme.
     * Count it.
     * The sentence that started "There are three."  What was the rest?
     * Russians exploded an A-bomb.
     * Moisture! Moisture!
     (159 more replies)...

Line:  Thanks.
Replies:
     * Could you maybe not tell anyone about this?
     * You`re a grungy little phoenix, you know that? Keep up the good work.
     * How was huntin`?
     * Sure. No problem.
     * Begin main sequence. Mark at 10-9-8- 7-6-5-4-3-2-1-drop.
     * My pleasure.
     * Drink up, young man. It`ll make the whole seduction part less repugnant.
     * And, anyway, you sell yourself short. I can tell. There`s a lot of stuff going on in your brain. I can tell. My goal... can I tell you my goal?
     * Good luck on your mission, Sir. By Grabthar`s Hammer, by the Suns of Warvan I wish you-
     * How do you want to pay?
     (88 more replies)...

Line:  Really?
Reply: Yeah.

Line:  What`s that?
Reply: What?

Line:  Oh.
Replies:
     *
     * And the second rule about it is... you`re not supposed to talk about it. And the third rule...
     * The reason I can fly is because my molecular structure is so constituted that I have a lighter density under Earth`s gravitational force.
     * Will you help me out...?
     * Older than thirty?
     * Gimme a couple dabs on the tongue.
     * I don`t want to be your friend like this anymore.
     * Julia`s my second wife.
     * I`ve never met her. My mother set it up.
     * Are you going to deny it?
     (93 more replies)...

Line:  Yes, sir.
Replies:
     * Got a vehicle?  Head north on 54. When you get to New Mexico you`ve gone too far.
     * And tell him not to delay.
     * Do you understand me?
     * I was sort of wondering what you looked like. Sit down.
     * Found no skeletons?
     * If you`re right, Lewis, this Mason is one hell of a pro.  He must have a service record and a paper trail some-...
     * She`s refused to say a word to anyone. What made you think you could get her to talk?
     * I have taken an oath that each letter of this testament shall be executed...and by God, it shall be done!
     * Then find me some cigarettes.
     * Let me see your cash book, will you?
     (86 more replies)...
```
